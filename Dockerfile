FROM php:7.4-apache-buster
ARG UNAME=koba
ARG UID=4001
ARG GID=4001
RUN groupadd -g $GID -o $UNAME 
RUN useradd -m -u $UID -g $GID -o -s /bin/bash $UNAME 
RUN apt-get update && apt-get install -y \
        curl \
		iputils-ping \
		net-tools \
		libfreetype6-dev \
		libjpeg62-turbo-dev \
		libpng-dev \
	&& docker-php-ext-configure gd --with-freetype --with-jpeg \
	&& docker-php-ext-install -j$(nproc) gd
COPY index.php /var/www/html 
COPY ports.conf /etc/apache2
COPY 000-default.conf /etc/apache2/sites-enabled

RUN chgrp -R 0 /var/www/html/ && \
    chmod -R g=u /var/www/html/ 	
RUN chown -R $UID:0 /var/www/html/
USER $UID 
