# k8s-web-php-iac
## Description
L'image utilisée est la php 7.4 avec apache sous debian buster. 
Elle est adaptée et fonctionnelle pour Openshift. 
> Si vous souhaitez l'utiliser en dehors d'Openshift, changez le groupe root sur /var/www/html par celui de l'user créé (koba)

## Configuration
### User 
Un utilisateur nommé "koba" faisant partie des groupe 4001 et ayant l'uid 4001 est créé

### Packages 
On met à jour les repos et on installe :

- curl
- iputils-ping 
- net-tools 
- libfreetype6-dev 
- libjpeg62-turbo-dev 
- libpng-dev 


On installe aussi ceci via les commandes docker créé par php
- docker-php-ext-configure gd --with-freetype --with-jpeg 
- docker-php-ext-install -j$(nproc) gd

### Apache config
On copie nos fichiers : 
index.php : retourne l'ip du serveur sur lequel il se situe
ports.conf : changement du port 80 pour le 8080 ( bon pour le rootless )
000-default.conf : adapté pour le 8080 uniquement.

### Ownership
On change les ownership sur le /var/www/html . Adapté pour openshift ( groupe root)

### rootless
On définit notre user afin de ne pas utiliser le compte root
